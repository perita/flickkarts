package com.peritasoft.flickkarts;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.utils.Disposable;

public class SfxPlayer implements Disposable {
    private final FlickKarts game;
    private final Sound sfxFlick;
    private final Sound sfxLap;


    public SfxPlayer(FlickKarts game) {
        this.game = game;
        this.sfxFlick = Gdx.audio.newSound(Gdx.files.internal("sfx/flick1.wav"));
        this.sfxLap = Gdx.audio.newSound(Gdx.files.internal("sfx/lap.wav"));
    }

    public void playSfxFlick() {
        if (!game.isMuted()) {
            sfxFlick.play();
        }
    }

    public void playSfxLap() {
        if (!game.isMuted()) {
            sfxLap.play();
        }
    }


    @Override
    public void dispose() {
        sfxFlick.dispose();
        sfxLap.dispose();
    }
}
