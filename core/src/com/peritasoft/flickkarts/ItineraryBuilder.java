package com.peritasoft.flickkarts;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class ItineraryBuilder {
    private final Array<Itinerary.Checkpoint> checkpoints = new Array<Itinerary.Checkpoint>(false, 16, Itinerary.Checkpoint.class);
    private Vector2[] points;

    public void setPoints(Vector2[] points) {
        this.points = points;
    }

    public void addCheckpoint(final Itinerary.Checkpoint checkpoint) {
        checkpoints.add(checkpoint);
    }

    public Itinerary build() {
        if (points == null) {
            throw new IllegalArgumentException();
        }
        return new Itinerary(points, checkpoints.toArray());
    }
}
