package com.peritasoft.flickkarts;

@SuppressWarnings("FieldMayBeFinal")
public class Player {
    private int id;
    private String name;
    private KartColor color;

    @SuppressWarnings("unused")
    public Player() {
        this(-1, "Ghost", KartColor.BLACK);
    }

    public Player(int id, String name, KartColor color) {
        this.id = id;
        this.name = name;
        this.color = color;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public KartColor getColor() {
        return color;
    }

    public void setColor(KartColor color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Player{" +
                getId() +
                ':' + (name == null ? "null" : '\'' + name + '\'') +
                '}';
    }
}
