package com.peritasoft.flickkarts.items;


import com.peritasoft.flickkarts.Kart;

public interface Item {
    ItemIcon getIcon();
    void use(Kart kart);

    enum ItemIcon {
        BURST
    }
}
